package com.example.demo.system;

import java.util.Collection;

/**
 * Extend this interface to implement concrete storage options e.g. a database.
 * 
 * @author Anelia, Rui
 *
 */
public interface TokenRepository {

    /**
     * Saves a single token to the repository.
     * 
     * @param token the token to be stores
     * @param customerCpr customer owning that token
     */
    public void storeToken(Token token, String customerCpr);
    
    /**
     * Saves multiple tokens to the repository.
     * 
     * @param tokens a collection of tokens
     * @param customerCpr customer owning all tokens
     */
    public void storeTokens(Collection<Token> tokens, String customerCpr);
    
    /**
     * Stores an indication that this token has been used for a payment.
     * 
     * @param token token that has been used
     */
    public void markTokenUsed(Token token);
    
    /**
     * Indicates whether the token has alredy been used for a payment.
     * 
     * @param token token to be checked
     * @return true if the token has been used,
     *         false otherwise
     */
    public boolean isTokenUsed(Token token);
    
    /**
     * Indicates whether the token belongs to the customer.
     * It provides no information whether the token has been used before.
     * 
     * @param token token to be checked
     * @param customerCpr customer ID for the token
     * @return true if the token belongs to this customer,
     *          false otherwise
     */
    public boolean isOwnershipValid(Token token, String customerCpr);
    
}
