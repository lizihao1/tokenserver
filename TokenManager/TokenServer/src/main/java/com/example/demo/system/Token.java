package com.example.demo.system;

import java.util.UUID;

/**
 * An entity used to facilitate and identify one payment in DTUPay.
 * 
 * @author Hubert Baumeister
 *
 */
public class Token {

    /**
     * Randomly generated UUID for the token.
     */
    private String tokenId;
    
    public Token() {
        this.tokenId = UUID.randomUUID().toString();
    }
    
    public Token(String tokenId) {
    	this.tokenId = tokenId;
    }
    
    public String getTokenId() {
        return tokenId;
    }
    
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Token)) {
			return false;
		}
		Token otherToken = (Token) other;
		return tokenId.equals(otherToken.tokenId);
	}
	
	@Override
	public int hashCode() {
		return tokenId.hashCode();
	}
}
