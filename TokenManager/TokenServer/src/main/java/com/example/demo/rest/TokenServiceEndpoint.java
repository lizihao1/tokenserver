package com.example.demo.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import com.example.demo.system.*;

/**
 * 
 * @author Stuart, Bijan, Li Wang
 *
 */


@Path("/TokenService")
public class TokenServiceEndpoint {
	
	
	private static TokenManager tokenManager = new TokenManager(new InMemoryTokenRepository());	
	
	@GET
	@Path("test")
	@Produces("text/plain")
	public String test() {
		return "yoooooooo";
	}
	
	@POST
	@Path("tokens")
	@Consumes("application/json")
	@Produces("application/json")
	public List<Token> requestTokens(TokenRequest r) {
		return tokenManager.requestTokens(r.getCpr(), r.getNumber());
	}
	
	@POST
	@Path("tokens/{tokenId}")
	@Consumes("application/json")
	public void markTokenUsed(@PathParam("tokenId") String tokenId) {
		tokenManager.markTokenUsed(tokenId);
	}
	
	@GET
	@Path("tokens/{tokenId}")
	@Produces("text/plain")
	public boolean getTokenStatus(@PathParam("tokenId") String tokenId) {
		return tokenManager.getTokenStatus(tokenId);  
	}
	
}
