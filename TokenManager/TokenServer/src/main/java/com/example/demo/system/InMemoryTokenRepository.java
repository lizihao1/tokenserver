package com.example.demo.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A token repository implemented using hash maps.
 * 
 * The data in this repository is only kept until the server is stopped.
 * This instance is appropriate for system tests.
 * For more persistent options, use file storage or a database.
 * 
 * @author Anelia, Rui
 *
 */
public class InMemoryTokenRepository implements TokenRepository {
    
    /**
     * Stores whether a token has been used for a payment.
     */
    private Map<Token, Boolean> tokenStatus = new HashMap<>();
    
    /**
     * Maps a customer ID to all tokens assigned to it.
     */
    private Map<String, Collection<Token>> cprToTokens = new HashMap<>();

    @Override
    public void storeToken(Token token, String customerCpr) {
        storeTokenStatus(token);
        mapTokenToCustomer(token, customerCpr);
    }
    
    @Override
    public void storeTokens(Collection<Token> tokens, String customerCpr) {
        for (Token token : tokens) {
            storeTokenStatus(token);
        }
        mapTokenToCustomer(tokens, customerCpr);
    }
    
    private void storeTokenStatus(Token token) {
        tokenStatus.put(token, false);
    }
    
    private void mapTokenToCustomer(Token token, String customerCpr) {
        List<Token> tokenList = new ArrayList<>();
        tokenList.add(token);
        mapTokenToCustomer(tokenList, customerCpr);
    }
    
    private void mapTokenToCustomer(Collection<Token> tokens, String customerCpr) {
        if (!cprToTokens.containsKey(customerCpr)) {
            cprToTokens.put(customerCpr, tokens);
        } else {
            cprToTokens.get(customerCpr).addAll(tokens);
        }
    }

    @Override
    public void markTokenUsed(Token token) {
        tokenStatus.put(token, true);
    }

    @Override
    public boolean isTokenUsed(Token token) {
        return tokenStatus.get(token);
    }

    @Override
    public boolean isOwnershipValid(Token token, String customerCpr) {
        if (!cprToTokens.containsKey(customerCpr)) {
            return false;
        }
        
        return cprToTokens.get(customerCpr).contains(token);
    }


}
